#include <SDL2/SDL.h>

#include <stdio.h>
#include <stdbool.h>

bool SetupSDL( SDL_Window** window, SDL_Surface** screen, SDL_Rect dimensions )
{
    printf( "SetupSDL \n" );
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "ERROR INITIALIZING SDL! %s \n", SDL_GetError() );
        return false;
    }

    *window = SDL_CreateWindow( "SDL Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, dimensions.w, dimensions.h, SDL_WINDOW_SHOWN );
    if ( *window == NULL )
    {
        printf( "ERROR CREATING WINDOW! %s \n", SDL_GetError() );
        return false;
    }

    *screen = SDL_GetWindowSurface( *window );
    if ( *screen == NULL )
    {
        printf( "ERROR ACCESSING WINDOW SURFACE! %s \n", SDL_GetError() );
        return false;
    }

    return true;
}

void CleanupSDL( SDL_Window* window )
{
    printf( "CleanupSDL \n" );
    SDL_DestroyWindow( window );
    SDL_Quit();
}

int main()
{
    printf( "main \n" );
    SDL_Window* window = NULL;
    SDL_Surface* screen = NULL;
    SDL_Rect dimensions;
    dimensions.x = 0;
    dimensions.y = 0;
    dimensions.w = 400;
    dimensions.h = 240;

    bool success = SetupSDL( &window, &screen, dimensions );
    if ( !success )
    {
        printf( "FATAL ERROR \n" );
        return 1;
    }

    SDL_Color backgroundColor;
    backgroundColor.r = 200;
    backgroundColor.g = 200;
    backgroundColor.b = 255;

    screen = SDL_GetWindowSurface( window );

    SDL_Event event;

    bool done = false;
    while ( !done )
    {
        // Get inputs/events
        while ( SDL_PollEvent( &event ) != 0 )
        {
            if ( event.type == SDL_QUIT )
            {
                done = true;
            }
        }

        // DRAWING
        // Clear screen
        SDL_FillRect( screen, NULL, SDL_MapRGB( screen->format, backgroundColor.r, backgroundColor.g, backgroundColor.b ) ); // int SDL_FillRect(SDL_Surface * dst, const SDL_Rect * rect, Uint32 color);
        // Update window
        SDL_UpdateWindowSurface( window );
    }

    CleanupSDL( window );

    return 0;
}
