#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <stdio.h>
#include <stdbool.h>

int main()
{
    printf( "INITIALIZING SDL AND EXTENSIONS... \n" );

    SDL_Rect dimensions = { 0, 0, 400, 240 };

    bool setupSuccess = true;

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) { printf( "ERROR INITIALIZING SDL! %s \n", SDL_GetError() ); setupSuccess = false; }

    SDL_Window* window = SDL_CreateWindow( "SDL Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, dimensions.w, dimensions.h, SDL_WINDOW_SHOWN );
    if ( window == NULL ) { printf( "ERROR CREATING WINDOW! %s \n", SDL_GetError() ); setupSuccess = false; }

    SDL_Renderer* renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
    if ( renderer == NULL ) { printf( "ERROR CREATING RENDERER! %s \n", SDL_GetError() ); setupSuccess = false; }
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );

    int imgFlags = IMG_INIT_PNG;
    if ( !( IMG_Init( imgFlags ) & imgFlags ) ) { printf( "ERROR INITIALIZING PNG IMAGES! %s \n", IMG_GetError() ); setupSuccess = false; }

    if ( TTF_Init() == -1 ) { printf( "ERROR INITIALIZING TTF LIBRARY! %s \n", TTF_GetError() ); setupSuccess = false; }
    TTF_Font* font = TTF_OpenFont( "PressStart2P.ttf", 28 );
    if ( font == NULL ) { printf( "ERROR LOADING FONT! %s \n", SDL_GetError() ); setupSuccess = false; }

    if ( !setupSuccess ) { printf( "FATAL ERROR \n" ); return 1; }

    printf( "CREATING ASSETS... \n" );

    SDL_Color backgroundColor = { 200, 200, 255 };

    // Load a texture
    SDL_Surface* loadSurface = IMG_Load( "grass.png" );
    if ( loadSurface == NULL )
    {
        printf( "ERROR LOADING SURFACE! %s \n", IMG_GetError() );
        return 1;
    }
    SDL_Texture* txImage = SDL_CreateTextureFromSurface( renderer, loadSurface );
    if ( txImage == NULL )
    {
        printf( "ERROR CONVERTING SURFACE TO TEXTURE! %s \n", SDL_GetError() );
        SDL_FreeSurface( loadSurface );
        return 1;
    }
    SDL_FreeSurface( loadSurface );

    // Generate a texture from text
    SDL_Color textColor = { 100, 100, 200 };
    SDL_Surface* textSurface = TTF_RenderText_Solid( font, "Hello!", textColor );
    if ( textSurface == NULL )
    {
        printf( "ERROR RENDERING TEXT! %s \n", TTF_GetError() );
        return 1;
    }
    SDL_Texture* txText = SDL_CreateTextureFromSurface( renderer, textSurface );
    if ( txText == NULL )
    {
        printf( "ERROR CONVERTING SURFACE TO TEXTURE! %s \n", TTF_GetError() );
        return 1;
    }
    SDL_FreeSurface( textSurface );

    SDL_Event event;

    printf( "BEGINNING PROGRAM LOOP... \n" );
    bool done = false;
    while ( !done )
    {
        // Get inputs/events
        while ( SDL_PollEvent( &event ) != 0 )
        {
            if ( event.type == SDL_QUIT )
            {
                done = true;
            }
        }

        // DRAWING
        // Clear screen
        SDL_RenderClear( renderer );

        // Draw image
        SDL_RenderCopy( renderer, txImage, NULL, NULL );
        SDL_RenderCopy( renderer, txText, NULL, NULL );

        // Update window
        SDL_RenderPresent( renderer );
    }

    printf( "FREEING MEMORY AND CLOSING PROGRAM... \n" );

    SDL_DestroyTexture( txImage );
    SDL_DestroyTexture( txText );
    TTF_CloseFont( font );
    SDL_DestroyWindow( window );
    TTF_Quit();
    SDL_Quit();

    return 0;
}
