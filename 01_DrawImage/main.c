#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <stdio.h>
#include <stdbool.h>

int main()
{
    printf( "INITIALIZING SDL AND EXTENSIONS... \n" );

    SDL_Rect dimensions = { 0, 0, 400, 240 };

    bool setupSuccess = true;

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) { printf( "ERROR INITIALIZING SDL! %s \n", SDL_GetError() ); setupSuccess = false; }

    SDL_Window* window = SDL_CreateWindow( "SDL Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, dimensions.w, dimensions.h, SDL_WINDOW_SHOWN );
    if ( window == NULL ) { printf( "ERROR CREATING WINDOW! %s \n", SDL_GetError() ); setupSuccess = false; }

    SDL_Renderer* renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
    if ( renderer == NULL ) { printf( "ERROR CREATING RENDERER! %s \n", SDL_GetError() ); setupSuccess = false; }
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );

    int imgFlags = IMG_INIT_PNG;
    if ( !( IMG_Init( imgFlags ) & imgFlags ) ) { printf( "ERROR INITIALIZING PNG IMAGES! %s \n", IMG_GetError() ); setupSuccess = false; }

    if ( !setupSuccess ) { printf( "FATAL ERROR \n" ); return 1; }

    printf( "CREATING ASSETS... \n" );

    SDL_Color backgroundColor = { 200, 200, 255 };
    SDL_Color textColor = { 100, 100, 200 };

    // Load a texture
    SDL_Surface* loadSurface = IMG_Load( "grass.png" );
    if ( loadSurface == NULL )
    {
        printf( "ERROR LOADING SURFACE! %s \n", IMG_GetError() );
        return 1;
    }
    SDL_Texture* txImage = SDL_CreateTextureFromSurface( renderer, loadSurface );
    if ( txImage == NULL )
    {
        printf( "ERROR CONVERTING SURFACE TO TEXTURE! %s \n", SDL_GetError() );
        SDL_FreeSurface( loadSurface );
        return 1;
    }
    SDL_FreeSurface( loadSurface );

    SDL_Event event;

    printf( "BEGINNING PROGRAM LOOP... \n" );
    bool done = false;
    while ( !done )
    {
        // Get inputs/events
        while ( SDL_PollEvent( &event ) != 0 )
        {
            if ( event.type == SDL_QUIT )
            {
                done = true;
            }
        }

        // DRAWING
        // Clear screen
        SDL_RenderClear( renderer );

        // Draw image
        SDL_RenderCopy( renderer, txImage, NULL, NULL );

        // Update window
        SDL_RenderPresent( renderer );
    }

    printf( "FREEING MEMORY AND CLOSING PROGRAM... \n" );

    if ( txImage != NULL )
    {
        SDL_DestroyTexture( txImage );
    }

    SDL_DestroyWindow( window );
    TTF_Quit();
    SDL_Quit();

    return 0;
}
